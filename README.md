# Mockup API for partner binding


## Running the app

```bash
$ ./gen_skeleton.sh
```


## View document online

For Java user
GO TO : ```http://<your ip>:3004/api/v1/swagger-ui.html#```

Else
GO TO : ```http://<your ip>:3004/docs```