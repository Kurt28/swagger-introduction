#!/bin/bash

type="$1"
sourceFolder="codegen_source"
skeletonFolder="swagger-introduction"


# Check if source folder exists
if [ ! -d $sourceFolder ] ; then
   echo "$sourceFolder is not found"
   exit 1
fi

# Remove old folder and add a new one
if [ -d $skeletonFolder ] ; then
   rm -rf $skeletonFolder && echo "remove $skeletonFolder" || die "Failed to remove $skeletonFolder"
   mkdir $skeletonFolder && echo "create $skeletonFolder" || die "Failed to create $skeletonFolder"
else
   mkdir $skeletonFolder && echo "create $skeletonFolder" || die "Failed to create $skeletonFolder"
fi

cp -r $sourceFolder/api $skeletonFolder && echo "copy api folder" || die "Failed to copy api folder"
cp -r $sourceFolder/paths $skeletonFolder && echo "copy paths folder" || die "Failed to copy paths folder"
cp -r $sourceFolder/definitions $skeletonFolder && echo "copy definitions folder" || die "Failed to copy definitions folder"

# The following command used for automatically assign host ip address is verified only in MacOS
# Extract ip
# host="$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')"
# port="3004"

# repalce host ip
# sed -i '' 's/^host:.*$/host: "'"$host:$port"'"/g' $skeletonFolder/api/swagger_skeleton.yaml
# function die(){
#    echo "$1"
#    exit 1
# }

if [ "$type" = "java" ]; then
  cd $skeletonFolder && java -jar ../swagger-codegen-cli-2.2.3.jar generate -i api/swagger_skeleton.yaml -l spring
  mvn install
  java -jar target/swagger-spring-1.0.0.jar
else
  cd $skeletonFolder && java -jar ../swagger-codegen-cli-2.2.3.jar generate -i api/swagger_skeleton.yaml -l nodejs-server
  npm install
  npm start
fi
